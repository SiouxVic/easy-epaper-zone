#ifndef ZONE
#define ZONE

#include "Arduino.h"
#include "Epaper.h"

#define DEFAULT_THICKNESS 3

class Zone {

private:
  Epaper* display;
  int x, y, width, height;
  int center_x, center_y;
  int columns;
  int columns_width;

  int get_print_position_x(String string,int column);
  int get_print_position_y(String string);
  int get_column_start_x(int column);
  int get_column_center_x(int column);


public:
  float font_y_offset;
  Zone ();
  Zone (Epaper* display,int y,int height,int number_of_columns);
  Zone (Epaper* display,int x,int y,int height,int number_of_columns);
  Zone (Epaper* display,int number_of_columns = 1);
  void print(String string,int column);
  void underline(uint8_t column = 0,uint8_t line_thickness = DEFAULT_THICKNESS);
  void clear_decorations(int column,uint8_t line_thickness = DEFAULT_THICKNESS);
  void clear_column(int column);
  int get_columns();
  int get_center_x();
  int get_center_y();
  int get_x();
  int get_y();
  int get_width();
  int get_height();
  int get_bottom_y();

};

#endif /* end of include guard: Zone */
