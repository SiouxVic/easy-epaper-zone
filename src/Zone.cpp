#include "Zone.h"

Zone::Zone(){
  display = 0;
  x = 0;
  y = 0;
  height = 0;
  width = 0;
  columns = 0;
  columns_width = 0;
  center_x = 0;
  center_y = 0;
  font_y_offset = 0.00f;
}

Zone::Zone (Epaper* epaper,int number_of_columns) {
  display = epaper;
  x = 0;
  y = 0;
  height = display -> get_height();
  width = display -> get_width();
  columns = number_of_columns;
  columns_width = width / columns;
  center_x = width/2;
  center_y = height/2 + y;
  font_y_offset = 0.75f;
}

Zone::Zone(Epaper* epaper, int start_y, int zone_height, int number_of_columns){
  display = epaper;
  x = 0;
  y = start_y;
  height = zone_height;
  width = display -> get_width();
  columns = number_of_columns;
  columns_width = width / columns;
  center_x = width/2;
  center_y = height/2 + y;
  font_y_offset = 0.75f;
}

Zone::Zone(Epaper* epaper, int start_x, int start_y, int zone_height, int number_of_columns){
  Zone(epaper,start_y,zone_height,number_of_columns);
  x=start_x;
}

void Zone::print(String string = "", int column = 0){
  display->print_partial(
    get_print_position_x(string, column),
    get_print_position_y(string),
    string,
    font_y_offset);
}

void Zone::underline(uint8_t column, uint8_t line_thickness){
  display->fill_rect(get_column_start_x(column), get_bottom_y() - line_thickness, columns_width, line_thickness);
}

void Zone::clear_decorations(int column,uint8_t line_thickness){
  display->clear_rect(get_column_start_x(column), get_bottom_y() - line_thickness, columns_width, line_thickness);
}

void Zone::clear_column(int column){
  if (column >= columns){
    column = columns-1;
  }else if (column < 0){
    column = 0;
  }

  display->clear_rect(get_column_start_x(column),get_y(),columns_width,get_height());
}

int Zone::get_print_position_x(String string,int column = 0){
  boundaries b = display->getTextBounds(string, 0, 0);
  int position_x =  get_column_center_x(column) - (b.width / 2);

  if (position_x < get_column_start_x(column)){
    position_x = get_column_start_x(column);
  }

  return position_x;
}

int Zone::get_column_center_x(int column = 0){
  return (columns_width / 2) + get_column_start_x(column);
}

int Zone::get_print_position_y(String string){
  boundaries b = display->getTextBounds(string, 0, 0);
  return get_center_y() - (b.height / 2);
}

int Zone::get_column_start_x(int column = 0){
  return columns_width * column;
}

int Zone::get_columns(){
  return columns;
}

int Zone::get_center_y(){
  return center_y;
}

int Zone::get_center_x(){
  return center_x;
}

int Zone::get_width(){
  return width;
}

int Zone::get_height(){
  return height;
}

int Zone::get_bottom_y(){
  return y + height;
}

int Zone::get_x(){
  return x;
}

int Zone::get_y(){
  return y;
}
