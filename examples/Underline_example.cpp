#include <Arduino.h>
#include "Epaper.h"
#include "Zone.h"


//Example uses an esp32
#define EPD_BUSY     GPIO_NUM_15
#define EPD_RST      GPIO_NUM_13
#define EPD_DC       GPIO_NUM_14
#define EPD_CS       GPIO_NUM_5
#define EPD_CLK      GPIO_NUM_18
#define EPD_DIN      GPIO_NUM_23

Epaper epd = Epaper(EPD_CS, EPD_DC, EPD_RST, EPD_BUSY);

Zone header;

void setup() {
  Serial.begin(115200);

  Serial.println("Init display ...");
  epd.init();

  Serial.println("Init zone ...");
  header = Zone(&epd,0,40,3);

  header.print("Test",0);

}

void loop() {
    header.underline(0);
    delay(500);
    header.clear_decorations(0);
    delay(500);
}
