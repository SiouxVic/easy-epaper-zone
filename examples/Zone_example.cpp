#include <Arduino.h>
#include "Epaper.h"
#include "Zone.h"

//Example uses an esp32
#define EPD_BUSY     GPIO_NUM_15
#define EPD_RST      GPIO_NUM_13
#define EPD_DC       GPIO_NUM_14
#define EPD_CS       GPIO_NUM_5
#define EPD_CLK      GPIO_NUM_18
#define EPD_DIN      GPIO_NUM_23

Epaper epd = Epaper(EPD_CS, EPD_DC, EPD_RST, EPD_BUSY);

#define NUMBER_OF_COLUMNS 3
Zone header;

void setup() {
  Serial.begin(115200);

  Serial.print("Init display ...");
  epd.init();
  Serial.println("Done");

  Serial.print("Clear display ...");
  epd.clear();
  Serial.println("Done");

  Serial.print("Init zone ...");
  header = Zone(&epd,0,epd.get_height() * 0.2,NUMBER_OF_COLUMNS);
  Serial.println("Done");

  Serial.println(
    "Zone x: "      + String(header.get_x())        +
    " | y: "        + String(header.get_y())        +
    " | width: "    + String(header.get_width())    +
    " | height: "   + String(header.get_height())   +
    " | center y: " + String(header.get_center_y()) +
    " | center x: " + String(header.get_center_x()) +
    " | bottom y: " + String(header.get_bottom_y())
    );
}

void loop() {
  for (int i = 0; i < NUMBER_OF_COLUMNS; i++)
  {
    header.print("test",i);
    delay(1000);
    header.clear_column(i);
  }
}
